-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Окт 09 2020 г., 17:11
-- Версия сервера: 8.0.21-0ubuntu0.20.04.4
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60501khdd`
--

-- --------------------------------------------------------

--
-- Структура таблицы `bilet`
--

CREATE TABLE `bilet` (
  `id` int NOT NULL,
  `id_сеанса` int NOT NULL,
  `id_места` int NOT NULL,
  `ФИО` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `bilet`
--

INSERT INTO `bilet` (`id`, `id_сеанса`, `id_места`, `ФИО`) VALUES
(1, 1, 3, 'Храмов Денис Дмитриевич'),
(2, 1, 4, 'Хажин Ильшат Азатович'),
(3, 2, 10, 'Кулик Владимир Викторович'),
(4, 2, 9, 'Иванов Иван Иванович'),
(5, 3, 7, 'Храмов Дмитрий Владимирович'),
(6, 3, 2, 'Храмова Светлана Борисовна'),
(7, 4, 3, 'Храмова Мария Дмитриевна'),
(8, 5, 4, 'Страхов Дмитрий Юрьевич'),
(9, 9, 10, 'Зубенко Михаил Петрович'),
(10, 7, 4, 'Назаров Ибрагим Зульфатович');

-- --------------------------------------------------------

--
-- Структура таблицы `film`
--

CREATE TABLE `film` (
  `id` int NOT NULL,
  `наименование` varchar(255) NOT NULL,
  `продолжительность` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `film`
--

INSERT INTO `film` (`id`, `наименование`, `продолжительность`) VALUES
(1, 'Бегущий по лезвию', '02:37:18'),
(2, 'Безумный Макс', '02:24:26'),
(3, 'Хищник', '01:47:06'),
(4, 'Славные парни', '02:18:13'),
(5, 'Схватка', '02:57:54');

-- --------------------------------------------------------

--
-- Структура таблицы `mesto`
--

CREATE TABLE `mesto` (
  `id` int NOT NULL,
  `id_зала` int NOT NULL,
  `ряд` int NOT NULL,
  `номер` int NOT NULL,
  `ценовая_категория` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `mesto`
--

INSERT INTO `mesto` (`id`, `id_зала`, `ряд`, `номер`, `ценовая_категория`) VALUES
(1, 1, 1, 1, 'Низшая'),
(2, 2, 2, 21, 'Средняя'),
(3, 1, 1, 13, 'Высшая'),
(4, 3, 7, 14, 'Средняя'),
(5, 1, 5, 11, 'Высшая'),
(6, 2, 15, 6, 'Низшая'),
(7, 1, 3, 7, 'Средняя'),
(8, 2, 5, 8, 'Высшая'),
(9, 3, 1, 9, 'Низшая'),
(10, 3, 7, 10, 'Высшая');

-- --------------------------------------------------------

--
-- Структура таблицы `price`
--

CREATE TABLE `price` (
  `id` int NOT NULL,
  `id_сеанса` int NOT NULL,
  `ценовая категория` varchar(255) NOT NULL,
  `цена` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `price`
--

INSERT INTO `price` (`id`, `id_сеанса`, `ценовая категория`, `цена`) VALUES
(1, 1, 'Низшая', 150),
(2, 2, 'Средняя', 250),
(3, 3, 'Средняя', 250),
(4, 4, 'Высшая', 350),
(5, 5, 'Низшая', 150),
(6, 6, 'Низшая', 150),
(7, 10, 'Высшая', 350),
(8, 7, 'Средняя', 250),
(9, 8, 'Низшая', 150),
(10, 9, 'Средняя', 250);

-- --------------------------------------------------------

--
-- Структура таблицы `seans`
--

CREATE TABLE `seans` (
  `id` int NOT NULL,
  `id_зала` int NOT NULL,
  `id_фильма` int NOT NULL,
  `дата_и_время_начала` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `seans`
--

INSERT INTO `seans` (`id`, `id_зала`, `id_фильма`, `дата_и_время_начала`) VALUES
(1, 1, 1, '2020-10-06 12:20:00'),
(2, 2, 2, '2020-10-06 11:00:00'),
(3, 3, 3, '2020-10-06 12:00:00'),
(4, 4, 4, '2020-10-06 10:40:00'),
(5, 5, 5, '2020-10-06 08:30:00'),
(6, 1, 2, '2020-09-16 12:35:00'),
(7, 2, 3, '2020-08-26 19:45:00'),
(8, 3, 1, '2020-09-30 23:25:00'),
(9, 4, 5, '2020-05-27 11:55:00'),
(10, 5, 4, '2020-11-29 13:35:00');

-- --------------------------------------------------------

--
-- Структура таблицы `zal`
--

CREATE TABLE `zal` (
  `id` int NOT NULL,
  `наименование` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `zal`
--

INSERT INTO `zal` (`id`, `наименование`) VALUES
(1, 'Зал 1'),
(2, 'Зал 2'),
(3, 'Зал 3'),
(4, 'Зал 4'),
(5, 'Зал 5');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `bilet`
--
ALTER TABLE `bilet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_сеанса` (`id_сеанса`),
  ADD KEY `id_места` (`id_места`);

--
-- Индексы таблицы `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `mesto`
--
ALTER TABLE `mesto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_зала` (`id_зала`);

--
-- Индексы таблицы `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_сеанса` (`id_сеанса`);

--
-- Индексы таблицы `seans`
--
ALTER TABLE `seans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_фильма` (`id_фильма`),
  ADD KEY `id_зала` (`id_зала`);

--
-- Индексы таблицы `zal`
--
ALTER TABLE `zal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `bilet`
--
ALTER TABLE `bilet`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `film`
--
ALTER TABLE `film`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `mesto`
--
ALTER TABLE `mesto`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `price`
--
ALTER TABLE `price`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `seans`
--
ALTER TABLE `seans`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `zal`
--
ALTER TABLE `zal`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `bilet`
--
ALTER TABLE `bilet`
  ADD CONSTRAINT `bilet_ibfk_1` FOREIGN KEY (`id_сеанса`) REFERENCES `seans` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `bilet_ibfk_2` FOREIGN KEY (`id_места`) REFERENCES `mesto` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `mesto`
--
ALTER TABLE `mesto`
  ADD CONSTRAINT `mesto_ibfk_1` FOREIGN KEY (`id_зала`) REFERENCES `zal` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `price`
--
ALTER TABLE `price`
  ADD CONSTRAINT `price_ibfk_1` FOREIGN KEY (`id_сеанса`) REFERENCES `seans` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `seans`
--
ALTER TABLE `seans`
  ADD CONSTRAINT `seans_ibfk_1` FOREIGN KEY (`id_фильма`) REFERENCES `film` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `seans_ibfk_2` FOREIGN KEY (`id_зала`) REFERENCES `zal` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
